package com.ahmet.wiredistanbul;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    TextView t_h1,t_h2,t_h3,t_h4,t_h5;
    ImageView i_h1,i_h2,i_h3,i_h4,i_h5;

    private List<TextView> list_tv;
    private List<ImageView> list_iv;

    RelativeLayout lyt_1,lyt_2,lyt_3,lyt_4,lyt_5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tanitim();
        icerikLink=new ArrayList<>();
        baslikLink=new ArrayList<>();
        resimLink=new ArrayList<>();

        lyt_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Link"+icerikLink.get(0), Toast.LENGTH_SHORT).show();
                if (internet())sayfaGecis(0);

            }
        });
        lyt_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Link"+icerikLink.get(0), Toast.LENGTH_SHORT).show();
                if (internet())sayfaGecis(1);
            }
        });
        lyt_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Link"+icerikLink.get(0), Toast.LENGTH_SHORT).show();
                if (internet())sayfaGecis(2);
            }
        });
        lyt_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Link"+icerikLink.get(0), Toast.LENGTH_SHORT).show();
                if (internet())sayfaGecis(3);
            }
        });
        lyt_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Link"+icerikLink.get(0), Toast.LENGTH_SHORT).show();
                if (internet())sayfaGecis(4);
            }
        });

        listeDoldur();

        if (internet())new sayfaCek(getApplicationContext(),list_iv,list_tv).execute();
        else
            Toast.makeText(this, "İnternet bağlantını kontrol edip uygulamaya tekrar giriş yapın", Toast.LENGTH_SHORT).show();
    }
    public boolean internet(){
        internetVarmi varmi=new internetVarmi(this);
        if (varmi.kontrol())return true;
        else{
            Toast.makeText(this, "İnternet bağlantında sorun var galiba !", Toast.LENGTH_SHORT).show();
            return false;
        }
    }
    public void sayfaGecis(int i){
        Intent i2 = new Intent(MainActivity.this, HaberIcerigi.class);
        i2.putExtra("baslik", ""+baslikLink.get(i));
        i2.putExtra("resim", ""+resimLink.get(i));
        i2.putExtra("icerik", ""+icerikLink.get(i));

        startActivity(i2);
    }
    public List<String> baslikLink;
    public List<String> resimLink;
    public List<String> icerikLink;
    public void setListe(List<String> icerikLink,List<String> resimLink,List<String> baslikLink){
        this.icerikLink=icerikLink;
        this.baslikLink=baslikLink;
        this.resimLink=resimLink;
    }
    private void tanitim(){
        t_h1=(TextView)findViewById(R.id.tv_haber_1);
        t_h2=(TextView)findViewById(R.id.tv_haber_2);
        t_h3=(TextView)findViewById(R.id.tv_haber_3);
        t_h4=(TextView)findViewById(R.id.tv_haber_4);
        t_h5=(TextView)findViewById(R.id.tv_haber_5);

        i_h1=(ImageView)findViewById(R.id.iv_haber_1);
        i_h2=(ImageView)findViewById(R.id.iv_haber_2);
        i_h3=(ImageView)findViewById(R.id.iv_haber_3);
        i_h4=(ImageView)findViewById(R.id.iv_haber_4);
        i_h5=(ImageView)findViewById(R.id.iv_haber_5);

        lyt_1=(RelativeLayout)findViewById(R.id.lyt_1);
        lyt_2=(RelativeLayout)findViewById(R.id.lyt_2);
        lyt_3=(RelativeLayout)findViewById(R.id.lyt_3);
        lyt_4=(RelativeLayout)findViewById(R.id.lyt_4);
        lyt_5=(RelativeLayout)findViewById(R.id.lyt_5);
    }
    private void listeDoldur(){
        list_iv=new ArrayList<>();
        list_tv=new ArrayList<>();

        list_iv.add(i_h1);
        list_iv.add(i_h2);
        list_iv.add(i_h3);
        list_iv.add(i_h4);
        list_iv.add(i_h5);

        list_tv.add(t_h1);
        list_tv.add(t_h2);
        list_tv.add(t_h3);
        list_tv.add(t_h4);
        list_tv.add(t_h5);
    }
    public  class sayfaCek extends AsyncTask<Void,Void,Void>{
        List<ImageView> l_iv;
        List<TextView> l_tv;
        Context context;
        List<String> linkler;
        List<String> resimler;
        List<String> basliklar;
        public sayfaCek(Context context, List<ImageView> l_iv,
                        List<TextView> l_tv){
            this.l_iv=l_iv;
            this.l_tv=l_tv;
            this.context=context;
            linkler=new ArrayList<>();
            resimler=new ArrayList<>();
            basliklar=new ArrayList<>();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try{

                final Document doc  = Jsoup.connect("https://www.wired.com").get();
                Elements elements = doc.select("div.cards-component__row");
                final Element firstDiv = elements.first();
                Element ikiDiv=elements.get(1);

                Elements fdiv_parcala=firstDiv.select("div.card-component--border");
                final Element ilk_haber=fdiv_parcala.first();
                //Log.e("haber 1",""+ilk_haber);


                    //Log.e("link 1",""+linkAl(ilk_haber));Log.e("resim 1",""+resimAl(ilk_haber));Log.e("açıklama 1",""+aciklamaAl(ilk_haber));

                //Log.e("haber 2',""+fdiv_parcala.get(1));
                Element iki_haber=fdiv_parcala.get(1);
                    //Log.e("link 2",""+linkAl(iki_haber));Log.e("resim 2",""+resimAl(iki_haber));Log.e("açıklama 2",""+aciklamaAl(iki_haber));


                Elements ikidiv_parcala=ikiDiv.select("div.cards-component__column");
                //Log.e("altParca",""+ikidiv_parcala);

                Elements ikidiv_ilkparca=ikidiv_parcala.select("div.card-component--left");
                //Log.e("ikidiv_ilk",""+ikidiv_ilkparca);

                Element uc_haber=ikidiv_ilkparca.first();
                //Log.e("haber 3",""+uc_haber);
                    //Log.e("link 3",""+linkAl(uc_haber));Log.e("resim 3",""+resimAl(uc_haber));Log.e("açıklama 3",""+aciklamaAl(uc_haber));

                //Log.e("haber 4",""+ikidiv_ilkparca.get(1));
                Element dort_haber=ikidiv_ilkparca.get(1);

                    //Log.e("link 4",linkAl(dort_haber));Log.e("resim 4",""+resimAl(dort_haber));Log.e("açıklama 4",""+aciklamaAl(dort_haber));
                Elements ikidiv_ikiparca=ikiDiv.select("div.card-component--text");
                //Log.e("haber 5",""+ikidiv_ikiparca);
                Element bes_haber=ikidiv_ikiparca.first();
                    //Log.e("link 5",linkAl(bes_haber));Log.e("resim 5",""+resimAl(bes_haber));Log.e("açıklama 5",""+aciklamaAl(bes_haber));
                final List<Element> haberler=new ArrayList<>();
                haberler.add(ilk_haber);
                haberler.add(iki_haber);
                haberler.add(uc_haber);
                haberler.add(dort_haber);
                haberler.add(bes_haber);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0;i<5;i++){
                            l_tv.get(i).setText(aciklamaAl(haberler.get(i)));
                            Glide.with(context)
                                    .load(resimAl(haberler.get(i)))
                                    .into(l_iv.get(i));
                            linkler.add(linkAl(haberler.get(i)));
                            resimler.add(resimAl(haberler.get(i)));
                            basliklar.add(aciklamaAl(haberler.get(i)));
                        }

                    }
                });

            }catch (Exception e){

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            setListe(linkler,resimler,basliklar);
        }
    }
    public String linkAl(Element gelen){
        Element h_2_link=gelen.select("a[href]").first();
        String link = h_2_link.attr("abs:href");
       // Log.e("link 2",""+link2);

        return link;
    }
    public String resimAl(Element gelen){
        Element h_1_resim=gelen.select("img[src]").first();
        String resim = h_1_resim.attr("srcset");
        //
        String[] d=resim.split("w,");
        if (d.length>2){
            //Log.e("resim 1",""+d[0]);
        }
        else {
            h_1_resim=gelen.select("source[srcset]").first();
            resim = h_1_resim.attr("srcset");
            d=resim.split("w,");
            //Log.e("resim 2",""+d[0]);

        }

        return d[0];

    }
    public String aciklamaAl(Element gelen){

        Element h_1_aciklama=gelen.select("a[href]").get(2);
        Elements kisa_aciklama=h_1_aciklama.select("h2");
        String kisa_acikla=kisa_aciklama.text();
        kisa_acikla=kisa_acikla.replace("<h2>","");
        kisa_acikla=kisa_acikla.replace("</h2>","");

        return kisa_acikla;
    }

}
