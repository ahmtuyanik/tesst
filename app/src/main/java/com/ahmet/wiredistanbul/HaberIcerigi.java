package com.ahmet.wiredistanbul;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

public class HaberIcerigi extends AppCompatActivity {
    private String baslik,resim,icerik;

    private ImageView iv;
    private TextView tv_baslik,tv_icerik,tv_ing_1,tv_ing_2,tv_ing_3,tv_ing_4,tv_ing_5
            ,tv_tr_1,tv_tr_2,tv_tr_3,tv_tr_4,tv_tr_5;
    private  List<TextView> l_tv_ing,l_tv_tr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haber_icerigi);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tanitimlar();

        iv=(ImageView)findViewById(R.id.iv_icerikResim);

        Bundle extras = getIntent().getExtras();
        if(extras == null) {
            baslik= null;
            resim= null;
        } else {
            baslik= extras.getString("baslik");
            resim=extras.getString("resim");
            icerik=extras.getString("icerik");

            tv_baslik.setText(baslik);
            Glide.with(getApplicationContext())
                    .load(resim)
                    .into(iv);
            Log.e("icerik ",""+icerik);
            new icerikCekme(icerik,tv_icerik,l_tv_ing,l_tv_tr).execute();
        }



    }
    public void tanitimlar(){
        l_tv_ing=new ArrayList<>();
        l_tv_tr=new ArrayList<>();

        tv_baslik=(TextView)findViewById(R.id.tv_baslik);
        tv_icerik=(TextView)findViewById(R.id.tv_aciklama);
        tv_ing_1=(TextView)findViewById(R.id.tv_ing_1);
        tv_ing_2=(TextView)findViewById(R.id.tv_ing_2);
        tv_ing_3=(TextView)findViewById(R.id.tv_ing_3);
        tv_ing_4=(TextView)findViewById(R.id.tv_ing_4);
        tv_ing_5=(TextView)findViewById(R.id.tv_ing_5);

        tv_tr_1=(TextView)findViewById(R.id.tv_tr_1);
        tv_tr_2=(TextView)findViewById(R.id.tv_tr_2);
        tv_tr_3=(TextView)findViewById(R.id.tv_tr_3);
        tv_tr_4=(TextView)findViewById(R.id.tv_tr_4);
        tv_tr_5=(TextView)findViewById(R.id.tv_tr_5);

        l_tv_ing.add(tv_ing_1);
        l_tv_ing.add(tv_ing_2);
        l_tv_ing.add(tv_ing_3);
        l_tv_ing.add(tv_ing_4);
        l_tv_ing.add(tv_ing_5);

        l_tv_tr.add(tv_tr_1);
        l_tv_tr.add(tv_tr_2);
        l_tv_tr.add(tv_tr_3);
        l_tv_tr.add(tv_tr_4);
        l_tv_tr.add(tv_tr_5);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();

            return true;
        }
        return false;
    }
    public class  icerikCekme extends AsyncTask<Void,Void,Void>{

        String link;
        TextView tv;
        List<String> l_ing_5=new ArrayList<>();
        List<Integer> l_sayilari=new ArrayList<>();
        List<TextView> l_tvler;
        List<TextView> l_tvler_tr;
        icerikCekme(String link,TextView tv,List<TextView> l_tvler
                ,List<TextView> l_tvler_tr){
            this.link=link;
            this.tv=tv;
            this.l_tvler=l_tvler;
            this.l_tvler_tr=l_tvler_tr;
        }
        @Override
        protected Void doInBackground(Void... voids) {

            try{

                final Document doc  = Jsoup.connect(""+link).get();
                Elements elements = doc.select("article.article-body-component");
                final Element firstDiv = elements.first();
                //Element ikiDiv=elements.get(1);
                //Log.e("fdiv",""+firstDiv);
                final Element d_1=firstDiv.select("div").first();

                final Elements d_2=d_1.select("p");
                //Log.e("d_2.size",""+d_2.size());
                //d_2.remove((d_2.size()-1));

                Map<String,Word> countMap = new HashMap<String,Word>();
                String alayi = null;
                for (Element e : d_2){
                    String clean = Jsoup.clean(e.html(), Whitelist.basic());
                    clean=Jsoup.clean(e.html(), Whitelist.simpleText());
                    clean=Jsoup.parse(clean).text();
                    clean=clean.replace("'","");
                    clean=clean.replace("\"","");
                    clean=clean.replace(".","");
                    clean=clean.replace("?","");
                    clean=clean.replace("”","");
                    clean=clean.replace("“","");
                    clean=clean.replace(",","");
                    alayi+=clean;

                    final String finalClean = clean;
                    runOnUiThread(new Runnable() {
                        @Override public void run() {tv.append("\n"+ finalClean +"\n\n");}});
                }
                kelimeYakala(alayi,countMap);

            }catch (Exception e){

                e.printStackTrace();
            }
            return null;
        }
        public void kelimeYakala(String gelen, Map<String,Word> countMap){
            try {

                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        new ByteArrayInputStream(gelen.getBytes(StandardCharsets.UTF_8))));
                String line;
                int i2=0;
                int j=0;
                while ((line = reader.readLine()) != null) {
                    String[] words = line.split(" ");
                    //Log.e("words.leng",""+words.length);
                    i2++;
                    for (String word : words) {

                        //Log.e("word ","i :"+i2+" j:"+j+"  "+word);
                        j++;
                        if ("".equals(word)) {
                            continue;
                        }

                        Word wordObj = countMap.get(word);
                        if (wordObj == null) {
                            wordObj = new Word();
                            wordObj.word = word;
                            wordObj.count = 0;
                            countMap.put(word, wordObj);
                        }

                        wordObj.count++;
                    }
                }

                reader.close();

                SortedSet<Word> sortedWords = new TreeSet<Word>(countMap.values());
                int i = 0;
                int maxWordsToDisplay = 10;

                String[] wordsToIgnore = {""};

                for (final Word word : sortedWords) {
                    if (i >= maxWordsToDisplay) {
                        break;
                    }
                    if (Arrays.asList(wordsToIgnore).contains(word.word)) {
                        i++;
                        maxWordsToDisplay++;
                    } else {
                        //Log.e("sayi :"+i+":",word.count + "\t" + word.word);
                        if (i<5){
                            l_ing_5.add(word.word);
                            l_sayilari.add(word.count);
                            final int finalI = i;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    l_tvler.get(finalI).setText(word.count+"- "+word.word+" :");
                                }
                            });
                        }
                        i++;
                    }

                }
            }catch (Exception e){

            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new kelimeCeviri(l_ing_5,l_tvler_tr).execute();
        }
    }
    public class kelimeCeviri extends  AsyncTask<Void,Void,Void>{

        List<String> l_ing_kelime;
        List<TextView> l_tr_Tv;
        ProgressDialog pd;
        kelimeCeviri(List<String> l_ing_kelime,List<TextView> l_tr_Tv){
            this.l_ing_kelime=l_ing_kelime;
            this.l_tr_Tv=l_tr_Tv;
            pd=new ProgressDialog(HaberIcerigi.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setTitle("Çeviri");
            pd.setMessage("Kelime Çevirileri Yükleniyor");
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try{

                for (int i=0;i<l_ing_kelime.size();i++) {


                    final Document doc = Jsoup.connect("https://tr.glosbe.com/en/tr/" + l_ing_kelime.get(i)).get();
                    //Log.e("link","https://tr.glosbe.com/en/tr/" + l_ing_kelime.get(i));
                    Elements elements = doc.select("strong.phr");
                    //Log.e("elements", ""+elements);

                    final Element firstDiv = elements.first();
                   // Log.e("bir",""+firstDiv);

                    Element ikiDiv = elements.get(1);
                   // Log.e("iki",""+ikiDiv);

                    String clean = Jsoup.clean(firstDiv.html(), Whitelist.basic());
                    clean=Jsoup.clean(firstDiv.html(), Whitelist.simpleText());

                    String c2 = Jsoup.clean(ikiDiv.html(), Whitelist.basic());
                    c2=Jsoup.clean(ikiDiv.html(), Whitelist.simpleText());

                    final String birlestir=clean+" , "+c2;
                    final int finalI = i;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.setMessage("durum :"+ finalI +" son :"+l_ing_kelime.size());
                            l_tr_Tv.get(finalI).setText(""+birlestir);
                        }
                    });



                }

            }catch (Exception e){

                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pd.dismiss();
            SystemClock.sleep(50);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(HaberIcerigi.this, "Çeviri işlemi tamamlandı", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    public static class Word implements Comparable<Word> {
        String word;
        int count;

        @Override
        public int hashCode() { return word.hashCode(); }

        @Override
        public boolean equals(Object obj) { return word.equals(((Word)obj).word); }

        @Override
        public int compareTo(Word b) { return b.count - count; }
    }
}

