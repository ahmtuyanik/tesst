package com.ahmet.wiredistanbul;

/**
 * Created by 4ristocrat on 3.12.2017.
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class internetVarmi{

    Context activity;
    public internetVarmi(Context activity){
        this.activity=activity;
    }
    public boolean kontrol(){
        boolean durum=false;
        ConnectivityManager connectivityManager = (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            durum = true;
        }
        else {
            try {
                if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ){
                    durum = true;
                }
                else durum = false;
            }catch (Exception e){

            }

        }

        return  durum;
    }
}